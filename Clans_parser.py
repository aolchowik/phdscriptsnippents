'''
line_arr
@author: anna
'''
import json
class Clans_parser:
    class format:no_files, text, json, text_and_json ='no_files', 'text', 'json', 'text_and_json' 
    def __init__(self, path, option='no_files'):
        self.option=option
        self.path = path
        self.hsp_array=[]#seq1    seq2    eval
        self.pos_array=[]#posx    posy    posz
        self.id_fastaName_sequence_array=[]#seq1 seq1_fastaName seq1_sequence
        self.gn_gc_gsl=[]#group_name group_color group_sequence_ids_list
        self.parse_clans_run(path)
        if self.format.json in self.option: self.data_to_json()
    def data_to_json(self,path):
        with open((path+".json"),"w") as json_dump:
            md={'hsp_array':self.hsp_array,'pos_array':self.pos_array,'gn_gc_gsl':self.gn_gc_gsl,'id_fastaName_sequence_array':self.id_fastaName_sequence_array}
            json.dump(md)
    def parse_clans_run(self, path):
        ext='.'+path.split('.')[-1]
        
        my_flag, line=('','')
        outfile_hsp, outfile_fasta, outfile_seqgroups,outfile_pos = \
        open(path.replace(ext, ".abc"),"w"), open(path.replace(ext, ".fasta"),"w"), open(path.replace(ext, ".seqgroup"),"w"), open(path.replace(ext, ".pos"),"w")
        
        switcher = {
            "<hsp>": (self.save_hsp, outfile_hsp),
            "<seq>": (self.save_fasta, outfile_fasta),
            "<seqgroups>": (self.save_group, outfile_seqgroups),
            "<pos>":  (self.save_position, outfile_pos),
        }
        #func = switcher.get(my_flag, lambda: "nothing")
    
        with open(path) as my_file:
           
            for line in my_file:
                if line.startswith("</"):  my_flag=""   
                elif line.startswith("<hsp>"):
                    my_flag="<hsp>"
                elif line.startswith("<seq>"):my_flag="<seq>"
                elif line.startswith("<seqgroups>"):my_flag="<seqgroups>"
                elif line.startswith("<pos>"):my_flag="<pos>"
                if my_flag not in line and my_flag!="":
                    func , file_obj = switcher.get(my_flag, lambda: "nothing")
                    func(line , file_obj)
                      
        outfile_hsp.close()
        outfile_seqgroups.close()
        outfile_fasta.close()
    '''    
    Use all to check if all strings/nums are in the list
    def hsp_array_has_connection(self, seq_a, seqb):
        seq_list=(seq_a, seqb)
        for hsp_array_row in self.hsp_array:
            if all(seqs in hsp_array_row for seqs in seq_list):
                pass
    '''          
        
    def save_hsp(self, line, outfile):
        """Squares x.
        >>> cp = new Clans_parser()
        >>> cp.save_hsp(self, line, outfile)
        """
        #print line
        line=line.replace(":"," ")
        line_arr=line.strip().split(" ");
        if len(line_arr)>3:
            #line_arr[2:]#  gets all the similarity values       
            # map(float, line_arr[2:])#converts str like 1e-12 to float
            # min gets the minimum eval           
            min_eval_value = str(min(map(float, line_arr[2:])))#converts str line 1e-12 to float
            line_arr[2]=min_eval_value
        line=line_arr[0]+" "+line_arr[1]+" "+line_arr[2]+"\n"
        self.hsp_array.append([line_arr[0],line_arr[1],line_arr[2]])
        if self.format.text in self.option: outfile.write(line)


        
    def save_fasta(self, line, outfile):       
        #correct for faulty Rsubunits names
        if '>Rebase.' in line: line=line.replace('>Rebase.','>Rebase_R.')
        
        if'>' in line: self.id_fastaName_sequence_array.append([line.strip()])
        else: 
            mylist=self.id_fastaName_sequence_array[-1]
            mylist.append(line.strip())
            self.id_fastaName_sequence_array[-1]=mylist
        if self.format.text in self.option: outfile.write(line)
        
    def save_group(self, line, outfile):
        if self.format.text in self.option: outfile.write(line)
        line=line.strip()
        if 'name' in line: self.gn_gc_gsl.append([line.replace('name=','')])
        elif 'color' in line:
            mylist=self.gn_gc_gsl[-1]
            mylist.append(line.replace('color=',''))
            self.gn_gc_gsl[-1]=mylist
        elif 'numbers' in line:    
            mylist=self.gn_gc_gsl[-1]
            mylist.append(line.replace('numbers=','').split(';'))
            self.gn_gc_gsl[-1]=mylist
       
    def save_position(self, line, outfile):
        if self.format.text in self.option: outfile.write(line)
        self.pos_array.append(line.strip().split(' ')[1:])#seq1    posx    posy    posz


if __name__ == '__main__':
    cp = Clans_parser('test_data/total_meth_cut_with_cluster7_hiearch_num.run',Clans_parser.format.json)
    
    #import doctest
    #doctest.testmod()

    